#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <queue>
#include <iterator>
#include <vector>
#include <sstream>
#include <algorithm>
#include "Pacote.h"
#include "Roteador.h"
#include <typeinfo>

using namespace std;

// CALCULA A PORTA DE SAÍDA DO DADO (PACOTE)
int porta_saida (string, string);

// ROTORNA A POSIÇÃO NA MATRIZ DO IP EM QUESTÃO EM FORMATO DE VETOR 
vector<int> getXY (string);

// ROTORNA A POSIÇÃO NA MATRIZ DO IP EM QUESTÃO EM FORMATO DE STRING
static string getOrderedPar(const string& ip);

// RECEBE A MATRIZ DE ROTEADOR, O PAR ORDENADO DA ORIGEM E A PORTA ONDE OS PACOTES ESTÃO ARMAZENADOS
// ESTA FUNÇÃO RETIRA OS PACOTES DO BUFFER, PÕE NA SAÍDA DO ROTEADOR ORIGEM E PÕE NA ENTRADA DO PRÓXIMO ROTEADOR E 
int sendPackage(Roteador**, int, int, int);
int main() {

    int cabecalho, terminador, counter = 1;
    std::string::size_type sz;

    /* TESTANDO CRIAÇÃO DE NOVOS PACOTES E ADIÇÃO NO BUFFER
    auto *c1 = new Pacote(1, 0, "oi", "9");
    buffer.push(*c1);
    auto *c2 = new Pacote(0, 0, "oi", "9");
    buffer.push(*c2);
    auto *c3 = new Pacote(0, 0, "oi", "9");
    buffer.push(*c3);
    auto *c4 = new Pacote(0, 1, "oi", "9");
    buffer.push(*c4);

    Porta *portas;
    portas = (Porta*)malloc(5*sizeof(Porta));

    portas[0].entrada = buffer;
    portas[0].saida = *c4;*/

    auto **routers = new Roteador *[3]; // CRIAÇÃO DE POINTER OF POINTER DE ROTEADORES E 1ª ALOCAÇÃO
    for (int i = 0; i < 3; ++i) routers[i] = new Roteador[3]; // 2ª ALOCAÇÃO
    auto *portas = (Porta*)malloc(5*sizeof(Porta)); // CRIAÇÃO DE VETOR DE 5 PORTAS
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            routers[i][j].setIP("192.168.0." + to_string(counter)); // SETANDO IP EM CADA ROTEADOR
            routers[i][j].setPortas(portas);
            counter++;
        }
    }
    // APÓS ESTA PARTE DO CÓDIGO FOI CRIADO A MATRIZ DE ROTEADORES E EM CADA ROTEADOR FOI SETADO UM IP E UM VETOR DE 5 PORTAS

    ifstream file("data.txt");
    if (file.is_open()) {
        string line;
        while (getline(file, line)) { // LOOP DE CADA LINHA DO ARQUIVO DE LEITURA
            queue<Pacote> buffer; // CRIAÇÃO DE BUFFER => FILA DE PACOTES
            istringstream iss(line); // SEPARA CADA PALAVRA DA LINHA
            vector<string> resultsLine(istream_iterator<string>{iss}, istream_iterator<string>()); // ORGANIZA CADA INFORMAÇÃO LIDA NUM _VECTOR_
            /**
             * - ORIGEM = resultsLine[0];
             * - DESTINO = resultsLine[1];
             * - QUANTIDADE = resultsLine[2]
             * - DADO = resultsLine[3];
             */
            // LOOP PARA CRIAÇÃO DE UMA CERTA QUANTIDADE DE PACOTES
            for (int i = 0; i < stoi(resultsLine[2], &sz); ++i) {
                // LÓGICA PARA CABEÇALHO
                cabecalho = (i == 0) ? 1 : 0;
                // LÓGICA PARA TERMINADOR
                terminador = (i == (stoi(resultsLine[2], &sz)-1)) ? 1 : 0;
                // CRIAÇÃO DE PACOTE
                auto *p = new Pacote(cabecalho, terminador, (resultsLine[3]+to_string(i)), resultsLine[1]);
                // ADIÇÃO DE PACOTE RECÉM CRIADO NO BUFFER
                buffer.push(*p);
            }
            // BUFFER CRIADO...
            
            // PRÓXIMO PASSO: SETAR O BUFFER NUMA DAS PORTAS DO ROTEADOR DE ORIGEM.
            
            // RECEBE O IP DE ORIGEM E RETORNA A POSIÇÃO NA MATRIZ REFERENTE AO IP DADO
            string pos = getOrderedPar(resultsLine[0]);
            // SEPARA POR PALAVRA
            istringstream ess(pos);
            // CRIA VETOR "PAR" COM O PAR DE ORDENADAS DA MATRIZ EM FORMATO DE STRING
            vector<string> par(istream_iterator<string>{ess}, istream_iterator<string>());
            
            int x = stoi(par[0], &sz);
            int y = stoi(par[1], &sz);
            
            // PEGANDO AS PORTAS DO ROTEADOR CUJO PAR ORDENADO É (X,Y)
            Porta* prt = routers[x][y].getPortas();
            // ATRIBUINDO O BUFFER NA PORTA 0 DO ROTEADOR DE ORIGEM
            prt[0].entrada = buffer;

            sendPackage(routers, x, y, 0);
        }
        file.close();
    }

    return 0;
}

// TODO TEM QUE MODIFICAR ESSA FUNCAO PARA ELA RECEBER COMO PARAMETRO TODOS OS ROTEADORES E O ROTEADOR ATUAL
// PARA CONSEGUIR MOVIMENTAR OS PACOTES VIA ROTEADORES, COM ISSO O PACOTE ATUAL SEMPRE TERA A INFOMACAO PARA
// ONDE O PACOTE TEM QUE SER ENVIADO.
int sendPackage(Roteador** roteadores, int x, int y, int iPort) {

    Roteador roteador = roteadores[x][y];

    Porta* porta = roteador.getPortas();

    // IMPRESSÃO DO IP E PAR ORDENADO DO ROTEADOR DE ORIGEM (VARIÁVEL)
    cout << "Origem: " << roteador.getIP() << ": ";
    string posAtual = getOrderedPar(roteador.getIP());
    cout << posAtual << endl;
    
    // IMPRESSÃO DO IP E PAR ORDENADO DO ROTEADOR DE DESTINO (NÃO VARIÁVEL)
    cout << "Destino: " << porta[iPort].entrada.front().getDestino() << ": ";
    string posDes = getOrderedPar(porta[iPort].entrada.front().getDestino()); 
    cout << posDes << endl;
    
    // DESCOBRE POR QUAL PORTA O PACOTE DEVE SAIR
    int caminho = porta_saida(posAtual, posDes);

    // FIM TEÓRICO DA RECURSÃO
    if (caminho == -1) { 
       cout << "Os pacotes chegaram no destino" << endl;
       return 0;
    }

    // IMPRIME A PORTA DE SAÍDA DO PACOTE
    cout << "Porta a pegar: " << caminho << endl;

    int aa = x, bb = y, newPort;
    cout << "Pacote >> " << iPort << endl;
    // LAÇO NO BUFFER PRESENTE NA PORTA DE ENTRADA DO ROTEADOR DE ORIGEM
    for(Pacote _p = porta[iPort].entrada.front(); porta[iPort].entrada.size() > 0; _p = porta[iPort].entrada.front())
    {
        cout << _p.getDados() << endl;
        Porta* portaDestino;
        // LÓGICA DE TROCA DE PORTAS: SE VIER DA PORTA 1 VAI PRA 3 E VISSE VERSA. SE VIER DA 2 VAI PRA 4 E VISSE VERSA...
        if (caminho == 1) {
            porta[1].saida = _p;
            aa = x-1;
            newPort = 3;
            portaDestino = roteadores[aa][bb].getPortas();
            portaDestino[newPort].entrada.push(porta[1].saida);
        } else if (caminho == 2) {
            porta[2].saida = _p;
            bb = y+1;
            newPort = 4;
            portaDestino = roteadores[aa][bb].getPortas();
            portaDestino[newPort].entrada.push(porta[2].saida);
        } else if (caminho == 3) {
            porta[3].saida = _p;
            aa = x+1;
            newPort = 1;
            portaDestino = roteadores[aa][bb].getPortas();
            portaDestino[newPort].entrada.push(porta[3].saida);
        } else if (caminho == 4) {
            porta[4].saida = _p;
            bb = y-1;
            newPort = 2;
            portaDestino = roteadores[aa][bb].getPortas();
            portaDestino[newPort].entrada.push(porta[4].saida);
        }
        porta[iPort].entrada.pop();
    }
    
    cout << "Pacotes no novo rotedor: " << roteadores[aa][bb].getIP() << endl;

    // TENTATIVA DE CHAMADA RECURSIVA DA FUNÇÃO PARA ENVIO ATÉ O ROTEADOR DE DESTINO (NÃO CONSEGUIMOS COLOCAR ESSA FUNCIONALIDADE)
    // return sendPackage(roteadores, aa, bb, newPort);
    return 0;
}

string getOrderedPar(const string& ip) {
    if (ip == "192.168.0.1") return "0 0";
    else if (ip == "192.168.0.2") return "0 1";
    else if (ip == "192.168.0.3") return "0 2";
    else if (ip == "192.168.0.4") return "1 0";
    else if (ip == "192.168.0.5") return "1 1";
    else if (ip == "192.168.0.6") return "1 2";
    else if (ip == "192.168.0.7") return "2 0";
    else if (ip == "192.168.0.8") return "2 1";
    else if (ip == "192.168.0.9") return "2 2";
    else return "";
}

vector<int> getXY(string pos){
    vector<int> xy;
    stringstream ss(pos);
    int temp;
    while (ss >> temp)
        xy.push_back(temp);
    
    return xy;
}

int porta_saida (string posAtual, string destino){
    vector<int> xyPos = getXY(posAtual);
    vector<int> xyDes = getXY(destino);

	if(xyDes[1] > xyPos[1]){
		return 2; // porta da direita
	}
	else if(xyDes[1] < xyPos[1]){
		return 4; // porta da esquerda
	}
	else if(xyDes[0] > xyPos[0]){
		return 3; // porta de baixo
	}
	else if(xyDes[0] < xyPos[0]){
		return 1; // porta de cima
	}
	else{
		return -1; // estar no destino
	}
};