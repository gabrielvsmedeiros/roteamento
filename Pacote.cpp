#include "Pacote.h"

Pacote::Pacote(int cabecalho, int terminador, string dados, string destino) {
    _cabecalho = cabecalho;
    _terminador = terminador;
    _dados = dados;
    _destino = destino;
}

Pacote::~Pacote() {

}

void Pacote::setDados(string dado) {
    _dados = dado;
}

string Pacote::getDados() {
    return _dados;
}

string Pacote::getDestino() {
    return _destino;
}