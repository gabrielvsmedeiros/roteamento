#ifndef PACOTE_H
#define PACOTE_H

#include <string>

using namespace std;

class Pacote {
public:
    void setDados(string dado);
    string getDados();
    string getDestino();
    Pacote(int cabecalho, int terminador, string dados, string destino);
    ~Pacote();

private:
    int _cabecalho;
    int _terminador;
    string _dados;
    string _destino;
};

#endif //PACOTE_H
