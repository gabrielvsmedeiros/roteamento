#include <fstream>
#include <iostream>
#include <utility>
#include "Roteador.h"

Roteador::Roteador(string ip, Porta *portas) {
    _ip = std::move(ip);
    _portas = portas;
}

Roteador::~Roteador() = default;

Roteador::Roteador() = default;

void Roteador::setIP(string ip) {
    _ip = std::move(ip);
}

string Roteador::getIP() {
    return _ip;
}

void Roteador::setPortas(Porta *portas) {
    _portas = portas;
}

Porta *Roteador::getPortas() {
    return _portas;
}
