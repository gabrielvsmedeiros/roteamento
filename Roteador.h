#ifndef UNTITLED_ROTEADOR_H
#define UNTITLED_ROTEADOR_H

#include <string>
#include <queue>
#include "Pacote.h"

using namespace std;

struct Porta {
    std::queue <Pacote> entrada;
    Pacote saida;
};

class Roteador {
public:
    void setIP(string ip);
    string getIP();
    void setPortas(Porta *portas);
    Porta *getPortas();

    /*string roteamento(Pacote pacote);*/
    Roteador(string ip, Porta *portas);
    Roteador();
    ~Roteador();

private:
    string _ip;
    Porta *_portas{};
};


#endif //UNTITLED_ROTEADOR_H
